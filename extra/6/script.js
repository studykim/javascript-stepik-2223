function testCycle(a, b) {
    min = a < b ? a : b
    max = a > b ? a : b

    let x = 0;
    for (let i = min; i <= max; i++) {
        x += i ** 2
    }
    return x;
}