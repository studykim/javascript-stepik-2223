function testCycle(n) {
    let x = []
    while (n > 0) {
        let digit = n % 2
        x.unshift(digit)
        n = (n - digit) / 2
    }
    return x.join("");
}