function testCycle(a, b) {
    min = a < b ? a : b
    max = a > b ? a : b

    let x = "";
    for (let i = min; i <= max; i++) {
        x += i + " "
    }

    return x.trim();
}