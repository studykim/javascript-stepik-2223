function testCycle(a, b) {
    min = a < b ? a : b
    max = a > b ? a : b

    let x = "";
    for (let i = max; i >= min; i--) {
        x += i + " "
    }

    return x.trim();
}