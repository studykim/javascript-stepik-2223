function testCycle(n) {
    let x = [0, 1];
    for (let i = 2; i < n; i++) {
        x.push(i);
    }
    for (let p = 2; p * p < n; p++) {
        for (let k = p; k * p < n; k++) {
            x[k * p] = -1;
        }
    }
    let res = "";
    for (let i = 2; i < x.length; i++) {
        if (x[i] !== -1) {
            res += i + " ";
        }
    }
    return res.trim();
}