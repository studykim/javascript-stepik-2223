function testCycle(n) {
    let x = 1;

    for (let i = n; i >= 1; i -= 2) {
        x *= i
    }
    return x;
}