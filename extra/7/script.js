function testCycle(n) {
    let x = "";
    let val = 0;
    for (let i = 1; i <= 2 * n - 1; i += 2) {
        val += i
        x += val + " "
    }

    return x.trim();
}