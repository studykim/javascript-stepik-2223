function testCycle(n) {
    if (n === 0) {
        return 0
    }
    if (n === 1) {
        return 1
    }
    return testCycle(n - 1) + testCycle(n - 2)
}