function testCycle(a, b) {
    let x = a < b ? a : b
    let y = a > b ? a : b

    while (y % x > 0) {
        let z = x
        x = y % x
        y = z
    }

    return x;
}