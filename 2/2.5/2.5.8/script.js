function testDateTime(a, b) {
    let date1 = new Date(Date.parse(a))
    let date2 = new Date(Date.parse(b))

    let resDate;
    if (date1.valueOf() > date2.valueOf()) {
        resDate = date1;
    } else {
        resDate = date2;
    }

    return ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'][resDate.getDay()]
}
